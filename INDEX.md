# Whiptail

Create TUI dialog boxes and forms and collect user input using libnewt. (Requires CWSDPMI)

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## WPTAIL.LSM

<table>
<tr><td>title</td><td>Whiptail</td></tr>
<tr><td>version</td><td>0.52.10a</td></tr>
<tr><td>entered&nbsp;date</td><td>2016-07-17</td></tr>
<tr><td>description</td><td>Create TUI  forms to collect user input using libnewt</td></tr>
<tr><td>keywords</td><td>dialog, whiptail, newt, gpm</td></tr>
<tr><td>author</td><td>Red Hat</td></tr>
<tr><td>maintained&nbsp;by</td><td>Red Hat, DOS port by Blairdude@gmail.com</td></tr>
<tr><td>platforms</td><td>Unix, DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU Library General Public License, Version 2</td></tr>
<tr><td>summary</td><td>Create TUI dialog boxes and forms and collect user input using libnewt. (Requires CWSDPMI)</td></tr>
</table>
